This is a collection of sparse matrices and meshes the PETSc team has collected over the years for testing and performance evaluation.

The matrix files can be read with lib/petsc/bin/PetscBinaryIO.py from the PETSc distribution or https://gitlab.com/petsc/petsc/-/blob/master/lib/petsc/bin/PetscBinaryIO.py

This should provide additional references to the formats of the matrices and meshes and tools (Python, Matlab) for reading them from disk.

